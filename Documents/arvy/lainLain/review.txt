Project Sangkur
Review v 0.01 december 16 2015

1.	Character
-	Player character ga ada backstory, motivasi, dan tujuan
-	Party member Perempuan_A & Laki-laki_A ga ada backstory, motivasi dan tujuan
-	Party member Perempuan_A & Laki-laki_A  status hubungannya apa?
-	Ga ada character progression (laki-laki_A and Perempuan_B, I�m lokking atcha�)
-	Kurang interaksi antar party member, party felt like a bunch of strangers


2.	Scenes
-	Intro ga menjelaskan player dari mana akan kemana, kenapa pingsan, kelaparan tidak dijelaskan dalam scene
-	Apa motivasi perempuan_A menolong player
-	Laki-laki_A benar-benar tidak dibahas sama sekali dalam cerita, tiba-tiba muncul dan tidak ada perkenalan
-	tidak ada alasan Laki-laki_A memperlihatkan peta harta karun pada player,  player belum menunjukkan kontribusi apa-apa pada saat ini, kepercayaan yang diberikan perempuan_A dan laki-laki_A tidak berdasar.
-	Begitu juga dengan diterimanya player menjadi party member, laki-laki_A dan perempuan_A tidak mengetahui agenda player, dan player sudah dibuktikan menjadi anggota party yang tidak kompeten (kalah melawan beruang) secara logis, saat ini player hanya menjadi beban bagi party.
-	Tujuan hidup player juga menjadi samar disini, kenapa player pada awalnya berjalan jauh sampai kelaparan dan capek, untuk sampai kemana dan dengan tujuan apa? Terus mengapa player tiba-tiba malah mau ikut berburu harta karun, bagaimana dengan agenda player sebelum bertemu perempuan_A dan laki-laki_A
-	Pada saat ini status player party adalah pemburu harta karun, menolong desa dari bandit tidak ada hubungannya dengan harta karun, kecuali mungkin clue bagi harta karun hanya bisa didapat dari desa tsb.
-	Kenapa perempuan_B hanya mengundang laki-laki_A saja untuk tinggal di desanya? Player dan perempuan_A tidak dianggap? Pada intro, perempuan_A seems to be the leading person of the 2, she acts more like a leader, (she talks, and make decision for both her and Laki-laki_A) and initiate actions, yet at the village, Laki-laki_A are given more affection than both of his teammate, why is this?
-	Interval serangan pada desa terlalu cepat, diserang bandit siang hari, malamnya diserang jin


3.	Whole Game
-	Less scene, more gameplay, this is supposed to be a generic RPG, not a storyteller
-	Everything goes too fast, player should do more things before the game progress
(Ex. More fields between the fallen girl and her village. give few days doing generic quests before the djinn attacked the village) 
-	Pay more attention to atmospheric factor, let player get to know more about the party members and/or npcs
-	THERE ARE TOO MANY TIMES THAT THE PLAYER RESTS, THE GAME ONLY RUNS FOR 10 MINUTES AND THE PLAYER ALREADY RESTS 5 TIMES! (unconscious, back to rest in the tent, resting after bonfire scene, resting after bandit attack, resting after celebration)
-	Overall this is a pretty good start for your first project without any knowledge of RPGMaker VX Ace, just keep on practicing and testplaying, stay patient and don�t give up! :)
